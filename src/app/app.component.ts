import * as firebase from 'firebase';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { Component, ViewChild } from '@angular/core';
import { DashPage } from '../pages/dash/dash';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { EventosPage } from  '../pages/eventos/eventos';
import { ShoppingPage } from '../pages/shopping/shopping';
import { NoticiasPage } from '../pages/noticias/noticias';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ConversasPage } from '../pages/conversas/conversas';

const config = {
  apiKey: "AIzaSyA-NHi-ocBOnB5ttjyiS0Flk0yf104-IKw",
  authDomain: "meg0-fcdb3.firebaseapp.com",
  databaseURL: "https://meg0-fcdb3.firebaseio.com",
  projectId: "meg0-fcdb3",
  storageBucket: "meg0-fcdb3.appspot.com",
  messagingSenderId: "258445842020",
  appId: "1:258445842020:web:c9d5a726df06ae55"
};

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = DashPage;

  pages: Array<{
    title: string, component: any
  }>;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {

    this.pages = [
      { title: 'DashBoard', component: DashPage },
      { title: 'Conversas', component: ConversasPage },
      { title: 'Eventos', component: EventosPage },
      { title: 'Shopping', component: ShoppingPage },
      { title: 'Notícias', component: NoticiasPage }
    ];

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      statusBar.overlaysWebView(false);
      splashScreen.hide();
    });
    firebase.initializeApp(config);
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
