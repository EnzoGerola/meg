import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { BatteryStatus } from '@ionic-native/battery-status';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NewsProvider } from '../providers/news/news';
import { StatusBar } from '@ionic-native/status-bar';
import { LongPressModule } from 'ionic-long-press';
import { Calendar } from '@ionic-native/calendar';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DashPage } from '../pages/dash/dash';
import { LoginPage } from '../pages/login/login';
import { EventosPage } from  '../pages/eventos/eventos';
import { ShoppingPage } from '../pages/shopping/shopping';
import { NoticiasPage } from '../pages/noticias/noticias';
import { ConversasPage } from '../pages/conversas/conversas';
import { EditEventosPage } from '../pages/edit-eventos/edit-eventos';
import { AddConversasPage } from '../pages/add-conversas/add-conversas';
import { AddItemShoppingPage } from '../pages/add-item-shopping/add-item-shopping';
import { PayPal, PayPalPayment, PayPalConfiguration,  PayPalPaymentDetails} from '@ionic-native/paypal';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DashPage,
    LoginPage,
    EventosPage,
    ShoppingPage,
    NoticiasPage,
    ConversasPage,
    EditEventosPage,
    AddConversasPage,
    AddItemShoppingPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    LongPressModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DashPage,
    LoginPage,
    EventosPage,
    ShoppingPage,
    NoticiasPage,
    ConversasPage,
    EditEventosPage,
    AddConversasPage,
    AddItemShoppingPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BatteryStatus,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Calendar,
    PayPal,
    NewsProvider
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
