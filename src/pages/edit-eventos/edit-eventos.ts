import { Component } from '@angular/core';
import { Calendar } from '@ionic-native/calendar';
import { IonicPage, NavController, NavParams, AlertController, Platform } from 'ionic-angular';

/**
* Generated class for the EditEventosPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-edit-eventos',
  templateUrl: 'edit-eventos.html',
})
export class EditEventosPage {

  calName = '';
  events = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private calendar: Calendar,
              private alertCtrl: AlertController,
              private plt: Platform) {

    this.calName = navParams.get('name');

    if (this.plt.is('ios')) {
      this.calendar.findAllEventsInNamedCalendar(this.calName).then(data => {
        this.events = data;
      });
    } else if (this.plt.is('android')) {
      let start = new Date();
      let end = new Date();
      end.setDate(end.getDate() + 31);

      this.calendar.listEventsInRange(start, end).then(data => {
        this.events = data;
      });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditEventosPage');
  }

}
