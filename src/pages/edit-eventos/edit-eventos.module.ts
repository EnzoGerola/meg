import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditEventosPage } from './edit-eventos';

@NgModule({
  declarations: [
    EditEventosPage,
  ],
  imports: [
    IonicPageModule.forChild(EditEventosPage),
  ],
})
export class EditEventosPageModule {}
