import * as firebase from 'firebase';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AddItemShoppingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-item-shopping',
  templateUrl: 'add-item-shopping.html',
})
export class AddItemShoppingPage {

  data = {titulo : '', local : '', obs : '', valor : ''};
  ref = firebase.database().ref('shopping_itens/');

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddItemShoppingPage');
  }

  AddItem(){
    let newItem = this.ref.push();
    newItem.set({
      titulo : this.data.titulo,
      local : this.data.local,
      obs : this.data.obs,
      valor : this.data.valor
    });

    this.navCtrl.pop();
  }

}
