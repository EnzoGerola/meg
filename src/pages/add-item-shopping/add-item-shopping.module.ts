import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddItemShoppingPage } from './add-item-shopping';

@NgModule({
  declarations: [
    AddItemShoppingPage,
  ],
  imports: [
    IonicPageModule.forChild(AddItemShoppingPage),
  ],
})
export class AddItemShoppingPageModule {}
