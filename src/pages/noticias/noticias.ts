import { Component } from '@angular/core';
import { NewsProvider } from '../../providers/news/news';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

/**
* Generated class for the NoticiasPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-noticias',
  templateUrl: 'noticias.html',
})
export class NoticiasPage {

  public noticias: any;
  public result: any;

  constructor(public navCtrl: NavController, private alertCtrl: AlertController, public newsProv: NewsProvider) {
    this.GetNoticias();
  }

  ionViewDidLoad() {

  }

  GetNoticias(){
    this.newsProv.GetNoticiasTech()
    .then(result => {
      return this.noticias = result.articles;
    });
  }

}
