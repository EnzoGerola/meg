import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddConversasPage } from './add-conversas';

@NgModule({
  declarations: [
    AddConversasPage,
  ],
  imports: [
    IonicPageModule.forChild(AddConversasPage),
  ],
})
export class AddConversasPageModule {}
