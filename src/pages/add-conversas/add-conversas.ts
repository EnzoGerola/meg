import * as firebase from 'firebase';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AddConversasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-conversas',
  templateUrl: 'add-conversas.html',
})
export class AddConversasPage {

  data = {conv_nome : ''};
  ref = firebase.database().ref('chatrooms/');

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddConversasPage');
  }

  AddConversa(){
    let newConv = this.ref.push();
    newConv.set({
      conv_nome : this.data.conv_nome
    });

    this.navCtrl.pop();
  }

}
