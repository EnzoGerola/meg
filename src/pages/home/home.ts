import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content } from 'ionic-angular';
import { ConversasPage } from '../conversas/conversas';
import * as firebase from 'firebase';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild(Content) content: Content;

  data = { type : '', username : '', mensagem : '' };
  conversas = [];
  key : string;
  username : string;
  offStatus : boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.key = this.navParams.get("key") as string;
    this.username = this.navParams.get("username") as string;
    this.data.type = 'mensagem';
    this.data.username = this.username;

    let joinData = firebase.database().ref('chatrooms/'+this.key+'/conversas').push();
    joinData.set({
      type : 'join',
      username : this.username,
      sendDate : Date()
    });

    this.data.mensagem = '';

    firebase.database().ref('chatrooms/'+this.key+'/conversas').on('value', resp => {
      this.conversas = [];
      this.conversas = snapshotToArray(resp);
    });
  }

  enviarMsg(){
    let newData = firebase.database().ref('chatrooms/'+this.key+'/conversas').push();
    newData.set({
      type : this.data.type,
      user : this.data.username,
      mensagem : this.data.mensagem,
      sendDate : Date()
    });

    this.data.mensagem = '';
  }

  sairChat(){
    let newData = firebase.database().ref('chatrooms/'+this.key+'/conversas').push();
    newData.set({
      type : 'sair',
      user : this.data.username,
      mensagem : this.data.username + ' saiu do grupo',
      sendDate : Date()
    });

    this.offStatus = true;

    this.navCtrl.setRoot(ConversasPage, {
      username : this.username
    });
  }

}

export const snapshotToArray = snapshot => {
  let ArrayRet = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    ArrayRet.push(item);
  });

  return ArrayRet;
};
