import * as firebase from 'firebase';
import { Component } from '@angular/core';
import { EventosPage } from '../eventos/eventos';
import { ShoppingPage } from '../shopping/shopping';
import { ConversasPage } from '../conversas/conversas';
import { NoticiasPage } from '../noticias/noticias';
import { NewsProvider } from '../../providers/news/news';
import { BatteryStatus } from '@ionic-native/battery-status';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';

/**
 * Generated class for the DashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dash',
  templateUrl: 'dash.html',
})
export class DashPage {

  porc_batt : number;
  qtd_itens : number;
  qtd_convs : number;
  qtd_news  : number;
  db = firebase.database();
  ref_conversas = firebase.database().ref('chatrooms/');
  ref_shopping = firebase.database().ref('shopping_itens/');

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private batteryStatus: BatteryStatus,
              private plt: Platform,
              public newsProv: NewsProvider) {

    this.ref_shopping.on('value', resp => {
      this.qtd_itens = resp.numChildren();
    });

    this.ref_conversas.on('value', resp => {
      this.qtd_convs = resp.numChildren();
    });

    if(this.plt.is('ios') || this.plt.is('android')){
      this.batteryStatus.onChange().subscribe(status => {
       this.porc_batt = status.level;
      });
    }

    this.newsProv.GetNoticiasTech()
    .then(result => {
      return this.qtd_news = result.totalResults;
    });

  }

  ionViewDidLoad(){
  }

  PushConversas(){
    this.navCtrl.push(ConversasPage);
  }

  PushEventos(){
    this.navCtrl.push(EventosPage);
  }

  PushShopping(){
    this.navCtrl.push(ShoppingPage);
  }

  PushNews(){
    this.navCtrl.push(NoticiasPage);
  }


}
