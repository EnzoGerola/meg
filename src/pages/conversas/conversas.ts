import { Component } from '@angular/core';
import { LongPressModule } from 'ionic-long-press';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { AddConversasPage } from '../add-conversas/add-conversas';
import { HomePage } from '../home/home';
import * as firebase from 'firebase';

/**
 * Generated class for the ConversasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
export const snapshotToArray = snapshot => {
  let ArrayRet = [];

  snapshot.forEach(childSnapshot =>{
    ArrayRet.push({
      conv_nome : childSnapshot.val().conv_nome,
      key : childSnapshot.key
    });
  });

  return ArrayRet;
};

@IonicPage()
@Component({
  selector: 'page-conversas',
  templateUrl: 'conversas.html',
})

export class ConversasPage {

  conversas = [];
  el: HTMLElement;
  pressGesture: LongPressModule;
  db = firebase.database();
  ref = firebase.database().ref('chatrooms/');

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, public alertCtrl: AlertController) {
    this.ref.on('value', resp => {
      this.conversas = snapshotToArray(resp);
    });
  }

  MostrarToast(conv_nome : string){
    const toast = this.toastCtrl.create({
      message: 'Você entrou no Grupo ' + conv_nome,
      duration: 1000,
      position: 'top'
    });
    toast.present();
  }

  AddConversa(key){
    this.navCtrl.push(AddConversasPage);
  }

  EntrarConversa(key){
    //this.navParams.get('username')
    this.navCtrl.push(HomePage, {
      key : key,
      username : 'User'
    });
  }

  DeleteConversa(key){
    let alert = this.alertCtrl.create({
      title: 'Excluir Chat',
      message: 'Você está certo disso?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelado');
            this.navCtrl.push(HomePage);
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.db.ref('chatrooms/'+key).remove();
            const toast = this.toastCtrl.create({
              message: 'O chat foi excluído!',
              duration: 1000,
              position: 'bottom'
            });
            toast.present();
          }
        }
      ]
    });
    alert.present();
  }

}
