import { Component } from '@angular/core';
import { EditEventosPage } from '../edit-eventos/edit-eventos';
import { Calendar } from '@ionic-native/calendar';
import { IonicPage, NavController, NavParams, AlertController, Platform } from 'ionic-angular';

/**
* Generated class for the EventosPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-eventos',
  templateUrl: 'eventos.html',
})
export class EventosPage {

  calendars = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private calendar: Calendar,
    private alertCtrl: AlertController,
    private plt: Platform) {

      let date = new Date();
      this.plt.ready().then(() => {
        this.calendar.listCalendars().then(data => {
          this.calendars = data;
        });
      })
    }

    save(cal) {
      let date = new Date();
      let options = { calendarId: cal.id, calendarName: name, url: 'https://ionicacademy.com', firstReminderMinutes: 15 };

      this.calendar.createEventInteractivelyWithOptions('Titulo', 'Teste', 'Teste', date, date, options).then(res => {
        let alert = this.alertCtrl.create({
          title: 'Sucesso!',
          subTitle: 'Evento Criado.',
          buttons: ['OK']
        });
        alert.present();
        this.navCtrl.pop();
      }, err => {
        let alert = this.alertCtrl.create({
          title: 'Erro ao criar Evento!',
          subTitle: err,
          buttons: ['OK']
        });
        alert.present();
      });
      
    }

    openCal(cal) {
      this.navCtrl.push(EditEventosPage, {
        name: cal.name
      });
    }

  }
