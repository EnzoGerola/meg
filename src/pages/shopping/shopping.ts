import * as firebase from 'firebase';
import { Component } from '@angular/core';
import { LongPressModule } from 'ionic-long-press';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { AddItemShoppingPage } from '../add-item-shopping/add-item-shopping';
import { PayPal, PayPalPayment, PayPalConfiguration, PayPalPaymentDetails} from '@ionic-native/paypal';

/**
* Generated class for the ShoppingPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

export const snapshotToArray = snapshot => {
  let ArrayRet = [];

  snapshot.forEach(childSnapshot =>{
    ArrayRet.push({
      titulo : childSnapshot.val().titulo,
      local : childSnapshot.val().local,
      obs : childSnapshot.val().obs,
      valor : childSnapshot.val().valor,
      key : childSnapshot.key
    });
  });

  return ArrayRet;
};

@IonicPage()
@Component({
  selector: 'page-shopping',
  templateUrl: 'shopping.html',
})
export class ShoppingPage {

  itens = [];
  db = firebase.database();
  ref = firebase.database().ref('shopping_itens/');

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private payPal: PayPal,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController) {
    this.ref.on('value', resp => {
      this.itens = [];
      this.itens = snapshotToArray(resp);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShoppingPage');
  }

  addItem(){
    this.navCtrl.push(AddItemShoppingPage);
  }

  ComprarItem(titulo : string, valor){
    valor = valor.toString();
    this.payPal.init({
        PayPalEnvironmentProduction: '',
        PayPalEnvironmentSandbox: 'AdFooVoJD-JJTRxolApqxaIUajuYMG8TGYpwvl5ngeBer7xdF1Or4eZCOFOATlnv6efysAlQW13NPNTe'
    }).then(() => {
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        acceptCreditCards: true,
        languageOrLocale: 'pt-BR',
        merchantName: 'MEG',
        merchantPrivacyPolicyURL: '',
        merchantUserAgreementURL: ''
      })).then(() => {
        let detail = new PayPalPaymentDetails(valor, '0.00', '0.00');
        let payment = new PayPalPayment(valor, 'BRL', 'MEG', 'Sale', detail);
        this.payPal.renderSinglePaymentUI(payment).then((response) => {
          const toast = this.toastCtrl.create({
            message: 'Sua compra (R$'+valor+') do item ' + titulo + ' foi efetuada!',
            duration: 1000,
            position: 'bottom'
          });
          toast.present();
        }, () => {
          console.log('Erro ao renderizar o pagamento do paypal');
        })
      })
    })
  }

  DeleteItem(key){
    let alert = this.alertCtrl.create({
      title: 'Excluir Produto',
      message: 'Você está certo disso?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelado');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.db.ref('shopping_itens/'+key).remove();
            const toast = this.toastCtrl.create({
              message: 'O Produto foi excluído!',
              duration: 1000,
              position: 'bottom'
            });
            toast.present();
          }
        }
      ]
    });
    alert.present();
  }

}
