import { Http, Headers, RequestOptions, Response } from "@angular/http";
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
Generated class for the NewsProvider provider.

See https://angular.io/guide/dependency-injection for more info on providers
and Angular DI.
*/
@Injectable()
export class NewsProvider {

  public data: any;
  API_TOKEN = '0a04d08f592047bba28d6c25853f91bc';
  API_URL = 'https://newsapi.org/v2/top-headlines?country=br&category=technology&apiKey=';

  constructor(private http: Http) { }

  GetNoticiasTech(){
    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {

      this.http.get(`https://newsapi.org/v2/top-headlines?country=br&category=technology&apiKey=`+this.API_TOKEN)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

}
